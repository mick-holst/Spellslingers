/*
* Spellslingers
* Copyright (C) 2015  M.Holst
*
* Modified version of: Fruit Machine - The one armed bandit
* Copyright (C) 2014  B.Stultiens
* Modified May 18th 2015
*
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>

#include <defs.h>
#include <clock.h>

#define TMR0_PS		64UL					/* Timer prescaler 1:64 */
#define TMR0_FREQ	1000UL					/* Timer frequency */
#define TMR0CTC		(F_CPU/(TMR0_PS*TMR0_FREQ)-1UL)		/* CTC match value for frequency @ prescale*/

#define KEYPRESS_EV	0x10		/* Pressed a button */
#define KEYRELEASE_EV	0x20	/* Released a button */
#define TIMER_EV	0x30		/* A timer fired */
#define CAST_EV		0x40		/* End of rolling */
#define MUSIC_EV	0x50		/* End of tune */
#define FIND_EV					/* Find the spell */
#define EV_MASK		0xf0

#define IS_EV_KEYPRESS(x)	(((x) & EV_MASK) == KEYPRESS_EV)	/* Any keypress event */
#define IS_EV_KEYPRESSx(x,y)	((x) == (KEYPRESS_EV | (y)))		/* Specific keypress event */
#define IS_EV_KEYRELEASE(x)	(((x) & EV_MASK) == KEYRELEASE_EV)	/* Any keyrelease event */
#define IS_EV_TIMER(x)		(((x) & EV_MASK) == TIMER_EV)		/* Any timer event */
#define IS_EV_TIMERx(x,y)	((x) == (TIMER_EV | (y)))		/* Specific timer event */
#define IS_EV_CAST(x)	(((x) & EV_MASK) == CAST_EV)		/* End-of-roll event */
#define IS_EV_MUSIC(x)		(((x) & EV_MASK) == MUSIC_EV)		/* End of music tune event */
#define EV_DATA(x)		((x) & ~EV_MASK)			/* Data from the event */

#define SLEEP_TIME	60000		/* Time before we go to sleep */
#define SLEEPBLINK_TIME	2000		/* Low frequency blink to see we have power */
#define SLEEPBLINKHOLD_TIME 500		/* How long should the button blink */
#define START_MELODY_TIME 3500	/* Used when a new game starts */
#define RANDOMIZE_TIME 2000		/* Time it takes to randomize */
#define RANDOMIZE_BLINKTIME 666		/* Time between each blink on randoming */
#define LAUNCHREADY_TIME 250	/* Interval between LED state when ready to launch spell */
#define LAUNCH_TIME 1500		/* Time we want to see result, before doing the actual damage */
#define CELEBRATE_TIME 500		/* Interval between celebrate events */


uint8_t shiftdata[3];		/* Array used for setting the shift data */
const int Data = 0;
const int Latch = 1;
const int Clock = 2;

#define IS_HOLDING(x)	(holding & (1 << (x)))		/* Is a reel held? */

#define sound_off()	do { TCCR1A = _BV(COM1A1); /* Set clear output */ } while(0)

typedef struct __spellpart_t{
	uint16_t	element;
	uint8_t		disp;
}spellpart_t;

spellpart_t	curSpell[5];		/* Used for remembering the elements on displays, before sorting */
uint8_t		spellDmg;			/* the damage of the current spell */

enum {
	STATE_SLEEP,			/* All off, sleeping (most of the time) */
	STATE_START,			/* Starting a new game */
	STATE_PLAYPHASE,		/* Player interacts with the game */
	STATE_RANDOMIZE,		/* Get new random elements, for none locked segments */
	STATE_LAUNCHSPELL,		/* Spell is being fired at opponent */
	STATE_END,				/* Game ends and winner is celebrated */
};

static uint8_t state;			/* Current state of game */
static uint8_t holding;			/* Reel holding indicators */
static uint8_t blinkcnt;		/* Dot blinking counter */
static uint8_t lrdycnt;			/* Launch ready counter */
static uint16_t round;		/* allocates the current round */

static const uint16_t *musictune;	/* Melody to play */
static uint8_t musicidx;		/* Melody index */
static uint8_t playonce;		/* Flag for single play or looping */

static volatile uint16_t disps[10];	/* Display data */
static volatile uint8_t dispidx;	/* Current display index in scan */

#define BUTTON_HOLD1	0
#define BUTTON_HOLD2	1
#define BUTTON_HOLD3	2
#define BUTTON_HOLD4	3
#define BUTTON_HOLD5	4
#define BUTTON_ACTION	5 
#define BUTTON_DEBOUNCE	15		/* Button debounce time in timer-interrupts */

static volatile uint8_t bcnt[6];	/* Button debounce counters */
static volatile uint8_t bstate;		/* Button state */
#define BUTTONS_ALLOFF()	(bstate == 0x3f)
#define BUTTONS_HAS(x)		(!(bstate & (1<<((x)))))

static uint32_t rand_val;		/* PRGN state */
/* PRNG */
static void lfsrrandom(void)
{
	uint8_t c;
	cli();
	c = rand_val & 1;
	rand_val >>= 1;
	if (c)
		rand_val ^= 0xa6a6a6a6;
	sei();
}

static uint8_t randval(void)
{
	lfsrrandom();
	return rand_val & 0xff;
}


#define NEVENT	(1<<3)			/* Event queue size */
static uint8_t events[NEVENT];		/* Event queue */
static uint8_t evstart;			/* Tail of queue index */
static uint8_t evend;			/* Head of queue index */

uint8_t PlayerOne;
uint8_t PlayerTwo;

static void event_push(uint8_t ev)
{
	uint8_t sr = SREG;
	cli();
	if (((evstart + 1) % NEVENT) == evend) {
		SREG = sr;
		return;
	}
	events[evstart++] = ev;
	evstart %= NEVENT;
	((uint8_t *)&rand_val)[2] ^= ADCL & 0x01;	/* ADC noise as random source (LSB only) */

	SREG = sr;
}

uint8_t event_pop(void)
{
	uint8_t sr = SREG;
	uint8_t ev;
	cli();
	if (evstart == evend) {
		SREG = sr;
		return 0;
	}
	ev = events[evend++];
	evend %= NEVENT;

	if (IS_EV_KEYPRESS(ev) || IS_EV_KEYRELEASE(ev))
		((uint8_t *)&rand_val)[1] ^= TCNT2;	/* Mix event semi random into PRNG */

	SREG = sr;
	return ev;
}

/* Used for setting a certain display, with a certain value */
inline void setup_data(uint16_t row, uint8_t col)
{
	shiftdata[2] = 0x00;

	shiftdata[0] = row & 0xff;		
	shiftdata[1] = (row >> 4) & 0xf0;
	if (col < 4)
		shiftdata[1] |= 1 << col;
	else
		shiftdata[2] = 1 << (col - 4);
}

/* Shifts out an 8-bit chunk to the 75HC595's */
inline void ShiftOut8bit(uint8_t byteToShift)
{
	uint8_t i;

	for (i = 0; i < 8; i++)
	{
		PORTC &= ~(1 << Data);		/* Set data to low */

		if (0x80 & byteToShift) PORTC |= (1 << Data);		/* If we read a corelation, we set the data high */

		PORTC |= (1 << Clock);
		PORTC &= ~(1 << Clock);			/* Clock the data out */

		byteToShift <<= 1;			/* Shift the byte */
	}
}

/* Used for setting up all the displays, with the previously calculated values */
inline void setDisplay()
{
	/* To fix hardware problem, we make an empty clock to the display */
	PORTC &= ~(1 << Latch);
	ShiftOut8bit(0);
	ShiftOut8bit(0);
	ShiftOut8bit(0);
	PORTC |= (1 << Latch);

	/* Now we do the actual display set */
	PORTC &= ~(1 << Latch);	
	ShiftOut8bit(shiftdata[2]);
	ShiftOut8bit(shiftdata[1]);
	ShiftOut8bit(shiftdata[0]);
	PORTC |= (1 << Latch);	
}

#define CHECK_BUTTON(n)	do { \
				if(b & _BV((n))) { \
					bcnt[(n)] = BUTTON_DEBOUNCE; \
																																																																																																				} else if(bcnt[(n)]) { \
					if(!--bcnt[(n)]) \
						event_push((n) | ((bstate & _BV((n))) ? KEYRELEASE_EV : KEYPRESS_EV)); \
				} \
			} while(0)

/*
* Timer interrupt handling:
* - display scan
* - button scan+debounce
* - timer events
*/
ISR(TIMER0_COMPA_vect)
{
	uint8_t b;

	setup_data(disps[dispidx], dispidx);
	setDisplay();

	if (!dispidx)
		dispidx = 9;
	else
		dispidx--;

	/* Check buttons */
	b = bstate;
	b ^= bstate = PIND & 0x3f;	/* Check buttons, masked with those we have */
	for (uint8_t i = 0; i < 6; i++) {
		CHECK_BUTTON(i);
	}

	/* Update timers */
	for (uint8_t i = 0; i < MAXTIMERS; i++) {
		if (clock_timers[i].state == TIMER_RUNNING) {
			if (!--clock_timers[i].cnt) {
				clock_timers[i].state = TIMER_EXPIRED;
				event_push(TIMER_EV | i);
			}
		}
	}

	ADCSRA |= _BV(ADSC);	/* Start an ADC conversion */
}

/* Initialize CPU and peripherals */
static void setup(void)
{
	cli();

	dispidx = 0;

	MCUCR = 0;			/* Standby mode, no external ints */

	PORTB = ~_BV(1);
	PORTC = 0xf8;
	PORTD = 0xff;			/* Pull-up on buttons */

	DDRB = _BV(3) | _BV(2) | _BV(1) | _BV(0);
	DDRC = _BV(0) | _BV(1) | _BV(2);	/* Output for ShiftRegistre */
	DDRD = 0x00;

	TCCR0A = _BV(WGM01);		/* CTC mode */
	TCCR0B = _BV(CS01) | _BV(CS00);	/* timer0 prescaler 1:64 */
	OCR0A = TMR0CTC;		/* CTC value */

	TIMSK0 = _BV(OCIE0A);		/* Interrupt on compare A match */
	TCNT0 = 0;			/* Clear counter */

	sound_off();
	TCCR1B = _BV(WGM12) | _BV(CS10);
	OCR1A = pgm_read_word(&tone_pitch[TONE_C4]);
	TCNT1 = 0;

	/* Timer 2 is just spinning for random value */
	TCCR2A = 0;			/* Normal mode */
	TCCR2B = _BV(CS20);		/* timer2 prescaler 1:1 */

	ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX3);	/* Vref=1.1V, ADC8 (temp) */
	ADCSRA = _BV(ADEN) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADATE);	/* ADC enable, right adj, f/64 */
	ADCSRB = 0;					/* Free running ADC */

	evstart = evend = 0;
	bstate = PIND & 0x3f;

	rand_val = 0xdeadbeef;		/* PRNG cannot cope with zero */

	/* 16 segments, in order */
	disps[4] = 0;
	disps[7] = 0;
	disps[8] = 0;
	disps[9] = 0;
	disps[0] = 0;
	/* Round indicator LEDS */
	disps[3] = 0;
	/* 7 segments, in order */
	disps[5] = 0;
	disps[6] = 0;
	disps[2] = 0;
	disps[1] = 0;

	dispidx = 0;

	sei();				/* Enable interrupts */
}

/* Play a sound at pith tone for dura milliseconds */
void play(uint8_t tone, uint16_t dura)
{
	if (!tone || tone > NTONE_PITCH) {
		sound_off();
		return;
	}
	clock_timer_set(TIMER_NOTE, dura);
	OCR1A = pgm_read_word(&tone_pitch[tone - 1]);
	TCCR1A = _BV(COM1A0);	/* Set toggle output */
	TCNT1 = 0;
}

/* Play a melody */
static void tune_play(const uint16_t *t, uint8_t reps)
{
	playonce = reps;
	musictune = t;
	musicidx = 0;
	clock_timer_set(TIMER_MUSIC, 2);
}

/* Cancel melody play */
static void tune_cancel(void)
{
	musictune = NULL;
	clock_timer_clear(TIMER_MUSIC);
	sound_off();
}

/* Actions to perform when /entering/ a state */
static void state_enter(uint8_t s)
{
	uint8_t	i, j, flag = 1;
	spellpart_t temp;

	switch (s) {
	case STATE_SLEEP:
		disps[4] = 0;
		disps[7] = 0;
		disps[8] = 0;
		disps[9] = 0;
		disps[0] = 0;

		disps[3] = 0;

		disps[5] = 0;
		disps[6] = 0;
		disps[2] = 0;
		disps[1] = 0;

		blinkcnt = 0;
		clock_timer_set(TIMER_BLINK, SLEEPBLINK_TIME);
		break;

	case STATE_WAKING:

		break;

	case STATE_START:

		clock_timer_set(TIMER_START, START_MELODY_TIME);

		break;

	case STATE_PLAYPHASE:

		if (round == TRY_1)
		{
			round = TRY_2;
			disps[3] = TRY_2;
		}
		else if (round == TRY_2)
		{
			round = TRY_3;

			clock_timer_set(TIMER_LAUNCHREADY, LAUNCHREADY_TIME);

			disps[4] = disps[4] | (1 << 4);
			disps[7] = disps[7] | (1 << 4);
			disps[8] = disps[8] | (1 << 4);
			disps[9] = disps[9] | (1 << 4);
			disps[0] = disps[0] | (1 << 4);
			holding = 0x1f;
		}

		clock_timer_set(TIMER_SLEEP, SLEEP_TIME);
		break;

	case STATE_RANDOMIZE:
		if (!IS_HOLDING(0))
		{
			disps[4] = 0;
		}
		if (!IS_HOLDING(1))
		{
			disps[7] = 0;
		}
		if (!IS_HOLDING(2))
		{
			disps[8] = 0;
		}
		if (!IS_HOLDING(3))
		{
			disps[9] = 0;
		}
		if (!IS_HOLDING(4))
		{
			disps[0] = 0;
		}

		clock_timer_set(TIMER_BLINK, RANDOMIZE_BLINKTIME);
		clock_timer_set(TIMER_RANDOMIZER, RANDOMIZE_TIME);
		break;

	case STATE_LAUNCHSPELL:
		tune_play(tune_cast, 1);

		for (i = 0; i < 5; i++)
		{
			switch (i)
			{
			case 0:
				curSpell[0].element = disps[4] & ~(1 << 4);
				curSpell[0].disp = 4;
				break;

			case 1:
				curSpell[1].element = disps[7] & ~(1 << 4);
				curSpell[1].disp = 7;
				break;

			case 2:
				curSpell[2].element = disps[8] & ~(1 << 4);
				curSpell[2].disp = 8;
				break;

			case 3:
				curSpell[3].element = disps[9] & ~(1 << 4);
				curSpell[3].disp = 9;
				break;

			case 4:
				curSpell[4].element = disps[0] & ~(1 << 4);
				curSpell[5].disp = 0;
				break;
			}
		}

		for (i = 0; i < 5 && flag; i++)
		{
			flag = 0;

			for (j = 0; j < 4; j++)
			{
				if (curSpell[j + 1].element < curSpell[j].element)
				{
					temp = curSpell[j];
					curSpell[j] = curSpell[j + 1];
					curSpell[j + 1] = temp;
					flag = 1;
				}
			}
		}

		event_push(CAST_EV);
		break;

	case STATE_END:
		tune_play(tune_victory, 1);

		if (PlayerOne == 0)
		{
			disps[5] = 0;
			disps[6] = 0;
		}
		else
		{
			disps[2] = 0;
			disps[1] = 0;
		}

		clock_timer_set(TIMER_CELEBRATE, CELEBRATE_TIME);
		break;
	}
}

/* Actions to perform when /leaving/ a state */
static void state_leave(uint8_t s)
{
	switch (s) {
	case STATE_SLEEP:
		disps[4] = 0;
		clock_timer_clear(TIMER_SLEEP);
		break;

	case STATE_WAKING:

		break;

	case STATE_START:
		tune_cancel();

		round = 0;
		disps[3] = 0;

		PlayerOne = 50;
		disps[5] = NUM_5;
		disps[5] ^= (1 << 6);
		disps[6] = NUM_0;

		PlayerTwo = 50;
		disps[2] = NUM_5;
		disps[1] = NUM_0;

		clock_timer_clear(TIMER_START);
		break;

	case STATE_PLAYPHASE:
		clock_timer_clear(TIMER_SLEEP);
		break;

	case STATE_RANDOMIZE:

		break;

	case STATE_LAUNCHSPELL:
		clock_timer_clear(TIMER_LAUNCHREADY);

		spellDmg = 0;

		disps[5] ^= (1 << 6);
		disps[2] ^= (1 << 6);

		round = 0;
		disps[3] = 0;

		disps[4] = 0;
		disps[7] = 0;
		disps[8] = 0;
		disps[9] = 0;
		disps[0] = 0;

		holding = 0;

		break;

	case STATE_END:
		tune_cancel();

		disps[4] = 0;
		disps[7] = 0;
		disps[8] = 0;
		disps[9] = 0;
		disps[0] = 0;

		if (PlayerOne == 0)
		{
			disps[2] = pgm_read_word(&nums[PlayerTwo / 10]);
			disps[1] = pgm_read_word(&nums[PlayerTwo % 10]);
		}
		else
		{
			disps[5] = pgm_read_word(&nums[PlayerOne / 10]);
			disps[6] = pgm_read_word(&nums[PlayerOne % 10]);
		}

		clock_timer_clear(TIMER_SLEEP);
		break;
	}
}

/* Change state of the machine */
static void state_set(uint8_t s)
{
	state_leave(state);
	state = s;
	state_enter(s);
}

/* Event handling based on state */
static void state_machine(uint8_t key)
{
	uint8_t i;
	uint8_t prevHolding = holding;

	switch (state) {
	case STATE_SLEEP:
		if (IS_EV_TIMERx(key, TIMER_BLINK))
		{
			if (blinkcnt == 0)
			{
				disps[4] ^= (1 << 4);
				blinkcnt = 1;

				clock_timer_set(TIMER_BLINK, SLEEPBLINKHOLD_TIME);
			}
			else if (blinkcnt == 1)
			{
				disps[4] ^= (1 << 4);
				blinkcnt = 0;

				clock_timer_set(TIMER_BLINK, SLEEPBLINK_TIME);
			}
		}

		if (IS_EV_KEYPRESSx(key, BUTTON_HOLD1))
		{
			state_set(STATE_START);
		}
		break;

	case STATE_WAKING:

		break;

	case STATE_START:
		if (IS_EV_TIMERx(key, TIMER_START))
		{
			state_set(STATE_PLAYPHASE);
		}
		break;

	case STATE_PLAYPHASE:
		if (IS_EV_KEYPRESS(key))
		{
			clock_timer_clear(TIMER_SLEEP);
		}

		/* If play haven't roled yet */
		if (round == 0)
		{
			if (IS_EV_KEYPRESSx(key, BUTTON_ACTION))
			{
				play(TONE_A4, D_1_64);

				disps[4] = pgm_read_word(&elements[randval() % ELEMENTS]);
				disps[7] = pgm_read_word(&elements[randval() % ELEMENTS]);
				disps[8] = pgm_read_word(&elements[randval() % ELEMENTS]);
				disps[9] = pgm_read_word(&elements[randval() % ELEMENTS]);
				disps[0] = pgm_read_word(&elements[randval() % ELEMENTS]);

				round = TRY_1;
				disps[3] = TRY_1;
			}
		}
		/* If the player has initiated first roll */
		else if (round == TRY_2 || round == TRY_1)
		{
			if (IS_EV_KEYPRESS(key))
			{
				play(TONE_A4, D_1_64);
			}

			if (IS_EV_KEYPRESSx(key, BUTTON_ACTION))
			{
				if (holding != 0x1f)
				{
					state_set(STATE_RANDOMIZE);
				}
				else
				{
					state_set(STATE_LAUNCHSPELL);
				}
			}
			else if (IS_EV_KEYPRESSx(key, BUTTON_HOLD1))
			{
				holding ^= (1 << 0);
				disps[4] ^= (1 << 4);
			}
			else if (IS_EV_KEYPRESSx(key, BUTTON_HOLD2))
			{
				holding ^= (1 << 1);
				disps[7] ^= (1 << 4);
			}
			else if (IS_EV_KEYPRESSx(key, BUTTON_HOLD3))
			{
				holding ^= (1 << 2);
				disps[8] ^= (1 << 4);
			}
			else if (IS_EV_KEYPRESSx(key, BUTTON_HOLD4))
			{
				holding ^= (1 << 3);
				disps[9] ^= (1 << 4);
			}
			else if (IS_EV_KEYPRESSx(key, BUTTON_HOLD5))
			{
				holding ^= (1 << 4);
				disps[0] ^= (1 << 4);
			}

			if (prevHolding != holding)
			{
				if (holding == 0x1f)
				{
					disps[3] = L_RDY_3;
					clock_timer_set(TIMER_LAUNCHREADY, LAUNCHREADY_TIME);
				}
				else if (prevHolding == 0x1f)
				{
					disps[3] = round;
					clock_timer_clear(TIMER_LAUNCHREADY);
					lrdycnt = 0;
					prevHolding = 0;
				}
			}
		}
		else if (round == TRY_3)
		{
			if (IS_EV_KEYPRESSx(key, BUTTON_ACTION))
			{
				state_set(STATE_LAUNCHSPELL);
			}
		}

		
		if (IS_EV_TIMERx(key, TIMER_LAUNCHREADY))
		{
			if (holding == 0x1f)
			{
				disps[3] = pgm_read_word(&launchReady[lrdycnt]);
				lrdycnt++;
				if (lrdycnt == LaunchReadyLength)
					lrdycnt = 0;
				clock_timer_set(TIMER_LAUNCHREADY, LAUNCHREADY_TIME);
			}
			else
				clock_timer_clear(TIMER_LAUNCHREADY);
		}

		if (IS_EV_TIMERx(key, TIMER_SLEEP))
		{
			state_set(STATE_SLEEP);
		}

		break;

	case STATE_RANDOMIZE:
		if (IS_EV_TIMERx(key, TIMER_RANDOMIZER))
		{
			if (!IS_HOLDING(0))
			{
				disps[4] = pgm_read_word(&elements[randval() % ELEMENTS]);
			}
			if (!IS_HOLDING(1))
			{
				disps[7] = pgm_read_word(&elements[randval() % ELEMENTS]);
			}
			if (!IS_HOLDING(2))
			{
				disps[8] = pgm_read_word(&elements[randval() % ELEMENTS]);
			}
			if (!IS_HOLDING(3))
			{
				disps[9] = pgm_read_word(&elements[randval() % ELEMENTS]);
			}
			if (!IS_HOLDING(4))
			{
				disps[0] = pgm_read_word(&elements[randval() % ELEMENTS]);
			}

			state_set(STATE_PLAYPHASE);
		}

		if (IS_EV_TIMERx(key, TIMER_BLINK))
		{
			play(TONE_D5, D_1_32);

			if (!IS_HOLDING(0))
			{
				disps[4] ^= (1 << 4);
			}
			if (!IS_HOLDING(1))
			{
				disps[7] ^= (1 << 4);
			}
			if (!IS_HOLDING(2))
			{
				disps[8] ^= (1 << 4);
			}
			if (!IS_HOLDING(3))
			{
				disps[9] ^= (1 << 4);
			}
			if (!IS_HOLDING(4))
			{
				disps[0] ^= (1 << 4);
			}

			clock_timer_set(TIMER_BLINK, RANDOMIZE_BLINKTIME);
		}
		break;

	case STATE_LAUNCHSPELL:

		if (IS_EV_CAST(key))
		{

			uint16_t a, b, c, d, e;

			while (1)
			{
				a = pgm_read_word(&spells[i].elements[0]);
				b = pgm_read_word(&spells[i].elements[1]);
				c = pgm_read_word(&spells[i].elements[2]);
				d = pgm_read_word(&spells[i].elements[3]);
				e = pgm_read_word(&spells[i].elements[4]);

				if ((a == curSpell[0].element) &&
					(b == curSpell[1].element) &&
					(c == curSpell[2].element) &&
					(d == curSpell[3].element) &&
					(e == curSpell[4].element))
				{
					spellDmg = pgm_read_byte(&spells[i].damage);
					uint8_t k = pgm_read_byte(&spells[i].usedElements);

					for (uint8_t j = 0; j < 5; j++)
					{
						if (!(0x01 & k))
						{
							disps[curSpell[j].disp] = 0;
						}
						k >>= 1;
					}

					clock_timer_set(TIMER_LAUNCH, LAUNCH_TIME);
					break;
				}
				i++;

			}
		}

		if (IS_EV_TIMERx(key, TIMER_LAUNCH))
		{
			if (disps[5] & 0x40)
			{
				if ((PlayerTwo - spellDmg) <= 0)
				{
					PlayerTwo = 0;
				}
				else
				{
					PlayerTwo -= spellDmg;
				}

				disps[2] = pgm_read_word(&nums[PlayerTwo / 10]);
				disps[1] = pgm_read_word(&nums[PlayerTwo % 10]);
			}
			else if (disps[2] & 0x40)
			{
				if ((PlayerOne - spellDmg) <= 0)
				{
					PlayerOne = 0;
				}
				else
				{
					PlayerOne -= spellDmg;
				}

				disps[5] = pgm_read_word(&nums[PlayerOne / 10]);
				disps[6] = pgm_read_word(&nums[PlayerOne % 10]);
			}

			if (PlayerOne == 0)
			{
				state_set(STATE_END);
			}
			else if (PlayerTwo == 0)
			{
				state_set(STATE_END);
			}
			else
			{
				state_set(STATE_PLAYPHASE);
			}
		}

		if (IS_EV_TIMERx(key, TIMER_LAUNCHREADY))
		{
			disps[3] = pgm_read_word(&launchReady[lrdycnt]);
			lrdycnt++;
			if (lrdycnt == LaunchReadyLength)
				lrdycnt = 0;
			clock_timer_set(TIMER_LAUNCHREADY, LAUNCHREADY_TIME);
		}

		break;

	case STATE_END:
		if (IS_EV_TIMERx(key, TIMER_CELEBRATE))
		{
			if (PlayerOne == 0)
			{
				if (disps[2] == 0)
				{
					disps[2] = pgm_read_word(&nums[PlayerTwo / 10]);
					disps[1] = pgm_read_word(&nums[PlayerTwo % 10]);
				}
				else
				{
					disps[2] = 0;
					disps[1] = 0;
				}
			}
			else
			{
				if (disps[5] == 0)
				{
					disps[5] = pgm_read_word(&nums[PlayerOne / 10]);
					disps[6] = pgm_read_word(&nums[PlayerOne % 10]);
				}
				else
				{
					disps[5] = 0;
					disps[6] = 0;
				}
			}

			disps[4] = randval() + randval();
			disps[7] = randval() + randval();
			disps[8] = randval() + randval();
			disps[9] = randval() + randval();
			disps[0] = randval() + randval();


			clock_timer_set(TIMER_CELEBRATE, CELEBRATE_TIME);
		}

		if (IS_EV_KEYPRESS(key))
		{
			state_set(STATE_START);
		}

		if (IS_EV_TIMERx(key, TIMER_SLEEP))
		{
			state_set(STATE_SLEEP);
		}

		break;

	default:
		break;
	}
}


int main(void)
{
	uint8_t i;

	setup();

	state_set(STATE_START);

	for (;;) {
		if ((i = event_pop())) {
			/* Check for state independent timer events */
			if (IS_EV_TIMERx(i, TIMER_NOTE)) {
				sound_off();
				continue;
			}
			else if (IS_EV_TIMERx(i, TIMER_MUSIC)) {
				uint16_t note;
				if (!musictune)
					continue;	/* Race happens if cancelation gets a timer interrupt */
				note = pgm_read_word(&musictune[musicidx++]);
				if (!note) {
					event_push(MUSIC_EV);
					if (playonce)
						continue;
					musicidx = 1;
					note = pgm_read_word(&musictune[0]);
				}
				clock_timer_set(TIMER_MUSIC, _D(note) * TIMESTEP);
				play(_N(note), _D(note) * TIMESTEP - 1);
				continue;
			}
			else {
				/* If we get here, we have a state-dependent event to handle */
				state_machine(i);
			}
		}
		/* Go to sleep while we have nothing to do */
		sleep_enable();
		sleep_cpu();
	}
}
