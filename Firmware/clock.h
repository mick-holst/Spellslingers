/*
* Spellslingers
* Copyright (C) 2015  M.Holst
*
* Modified version of: Fruit Machine - The one armed bandit
* Copyright (C) 2014  B.Stultiens
* Modified May 18th 2015
*
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __FRUIT_CLOCK_H
#define __FRUIT_CLOCK_H

#include <avr/interrupt.h>

enum {
	TIMER_MUSIC,
	TIMER_START,
	TIMER_NOTE,
	TIMER_RANDOMIZER,
	TIMER_LAUNCHREADY,
	TIMER_LAUNCH,
	TIMER_CELEBRATE,
	TIMER_BLINK,
	TIMER_SLEEP,
	MAXTIMERS
};

enum {
	TIMER_IDLE = 0,
	TIMER_RUNNING,
	TIMER_EXPIRED
};

typedef struct __clock_timer_t {
	uint8_t		state;
	uint16_t	cnt;
} clock_timer_t;

extern volatile clock_timer_t clock_timers[MAXTIMERS];

#define clock_countdown(x)	do { if(clock_timers[x].state == TIMER_RUNNING) { \
					if(!--clock_timers[x].cnt) \
						clock_timers[x].state = TIMER_EXPIRED; \
					} \
				} while(0)

#define clock_timer_set(id,ms)		do { cli(); clock_timers[(id)].cnt = (!(ms) ? 1 : (ms)); clock_timers[(id)].state = TIMER_RUNNING; sei(); } while(0)
#define clock_timer_set_isr(id,ms)	do { clock_timers[(id)].cnt = ms; clock_timers[(id)].state = TIMER_RUNNING; } while(0)
#define clock_timer_expired(id)		(clock_timers[(id)].state == TIMER_EXPIRED)
#define clock_timer_idle(id)		(clock_timers[(id)].state == TIMER_IDLE)
#define clock_timer_clear(id)		do { cli(); clock_timers[(id)].state = TIMER_IDLE; sei(); } while(0)
#define clock_timer_clear_isr(id)	do { clock_timers[(id)].state = TIMER_IDLE; } while(0)

#endif
