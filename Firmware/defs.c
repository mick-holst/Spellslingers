/*
* Spellslingers
* Copyright (C) 2015  M.Holst
*
* Modified version of: Fruit Machine - The one armed bandit
* Copyright (C) 2014  B.Stultiens
* Modified May 18th 2015
*
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <stdint.h>
#include <avr/pgmspace.h>

#include <defs.h>

const uint16_t elements[ELEMENTS] PROGMEM = {
	FIRE, EARTH, WATER, AIR,
};

/* Numerical values */
const uint16_t nums[] PROGMEM = {
	NUM_0, NUM_1, NUM_2, NUM_3, NUM_4,
	NUM_5, NUM_6, NUM_7, NUM_8, NUM_9,
};

const uint16_t tries[] PROGMEM = {
	TRY_3, TRY_2, TRY_1,
};

const uint16_t launchReady[] PROGMEM = {
	L_RDY_1, L_RDY_2, L_RDY_3,
};

/* All the possible spells and their damage amount */
const spell_t spells[] PROGMEM = {
	SPELL(FIRE, FIRE, FIRE, FIRE, FIRE, 20, 0x1F),
	SPELL(FIRE, FIRE, FIRE, FIRE, EARTH, 12, 0xF),
	SPELL(FIRE, FIRE, FIRE, FIRE, WATER, 12, 0xF),
	SPELL(FIRE, FIRE, FIRE, FIRE, AIR, 12, 0xF),
	SPELL(FIRE, FIRE, FIRE, EARTH, EARTH, 12, 0x1F),
	SPELL(FIRE, FIRE, FIRE, EARTH, WATER, 6, 0x7),
	SPELL(FIRE, FIRE, FIRE, EARTH, AIR, 6, 0x7),
	SPELL(FIRE, FIRE, FIRE, WATER, WATER, 4, 0x1F),
	SPELL(FIRE, FIRE, FIRE, WATER, AIR,	6, 0x7),
	SPELL(FIRE, FIRE, FIRE, AIR, AIR, 16, 0x1F),
	SPELL(FIRE, FIRE, EARTH, EARTH, EARTH, 12, 0x1F),
	SPELL(FIRE, FIRE, EARTH, EARTH, WATER, 4, 0xF),
	SPELL(FIRE, FIRE, EARTH, EARTH, AIR, 4, 0xF),
	SPELL(FIRE, FIRE, EARTH, WATER, WATER, 2, 0x1B),
	SPELL(FIRE, FIRE, EARTH, WATER, AIR, 2, 0x3),
	SPELL(FIRE, FIRE, EARTH, AIR, AIR, 10, 0x1B),
	SPELL(FIRE, FIRE, WATER, WATER, WATER, 4, 0x1F),
	SPELL(FIRE, FIRE, WATER, WATER, AIR, 2, 0xF),
	SPELL(FIRE, FIRE, WATER, AIR, AIR, 10, 0x1B),
	SPELL(FIRE, FIRE, AIR, AIR, AIR, 16, 0x1F),
	SPELL(FIRE, EARTH, EARTH, EARTH, EARTH, 12, 0x1E),
	SPELL(FIRE, EARTH, EARTH, EARTH, WATER, 6, 0xE),
	SPELL(FIRE, EARTH, EARTH, EARTH, AIR, 6, 0xE),
	SPELL(FIRE,	EARTH,	EARTH,	WATER,	WATER,	10,	0x1E),
	SPELL(FIRE, EARTH, EARTH, WATER, AIR, 2, 0X6),
	SPELL(FIRE, EARTH, EARTH, AIR, AIR, 2, 0x1E),
	SPELL(FIRE, EARTH, WATER, WATER, WATER, 6, 0x1C),
	SPELL(FIRE, EARTH, WATER, WATER, AIR, 2, 0xC),
	SPELL(FIRE, EARTH, WATER, AIR, AIR, 2, 0x18),
	SPELL(FIRE, EARTH, AIR, AIR, AIR, 6, 0x1C),
	SPELL(FIRE, WATER, WATER, WATER, WATER, 12, 0x1E),
	SPELL(FIRE, WATER, WATER, WATER, AIR, 6, 0xE),
	SPELL(FIRE, WATER, WATER, AIR, AIR, 12, 0x1E),
	SPELL(FIRE, WATER, AIR, AIR, AIR, 6, 0X1C),
	SPELL(FIRE, AIR, AIR, AIR, AIR, 12, 0x1E),
	SPELL(EARTH, EARTH, EARTH, EARTH, EARTH, 20, 0x1F),
	SPELL(EARTH, EARTH, EARTH, EARTH, WATER, 12, 0xF),
	SPELL(EARTH, EARTH, EARTH, EARTH, AIR, 12, 0xF),
	SPELL(EARTH, EARTH, EARTH, WATER, WATER, 16, 0x1F),
	SPELL(EARTH, EARTH, EARTH, WATER, AIR, 6, 0x7),
	SPELL(EARTH, EARTH, EARTH, AIR, AIR, 4, 0x1F),
	SPELL(EARTH, EARTH, WATER, WATER, WATER, 16, 0x1F),
	SPELL(EARTH, EARTH, WATER, WATER, AIR, 10, 0xF),
	SPELL(EARTH, EARTH, WATER, AIR, AIR, 2, 0x1B),
	SPELL(EARTH, EARTH, AIR, AIR, AIR, 4, 0x1F),
	SPELL(EARTH, WATER, WATER, WATER, WATER, 12, 0x1E),
	SPELL(EARTH, WATER, WATER, WATER, AIR, 6, 0x1E),
	SPELL(EARTH, WATER, WATER, AIR, AIR, 4, 0x1E),
	SPELL(EARTH, WATER, AIR, AIR, AIR, 6, 0x1C),
	SPELL(EARTH, AIR, AIR, AIR, AIR, 12, 0x1E),
	SPELL(WATER, WATER, WATER, WATER, WATER, 20, 0x1F),
	SPELL(WATER, WATER, WATER, WATER, AIR, 12, 0xF),
	SPELL(WATER, WATER, WATER, AIR, AIR, 12, 0x1F),
	SPELL(WATER, WATER, AIR, AIR, AIR, 12, 0x1F),
	SPELL(WATER, AIR, AIR, AIR, AIR, 12, 0x1E),
	SPELL(AIR, AIR, AIR, AIR, AIR, 20, 0x1F), 
};

/* Tone pitch table */
const uint16_t tone_pitch[NTONE_PITCH] PROGMEM = {
	61155, 57722, 54482, 51424, 48538, 45814, 43242, 40815, 38524, 36362, 34321, 32395,
	30577, 28860, 27240, 25711, 24268, 22906, 21620, 20407, 19261, 18180, 17160, 16197,
	15288, 14429, 13619, 12855, 12133, 11452, 10809, 10203, 9630, 9089 /* == 440Hz */, 8579, 8098,
	7643, 7214, 6809, 6427, 6066, 5725, 5404, 5101, 4814, 4544, 4289, 4048,
	3821, 3606, 3404, 3213, 3032, 2862, 2701, 2550, 2406, 2271, 2144, 2023,
	1910, 1802, 1701, 1606, 1515, 1430, 1350, 1274, 1202, 1135, 1071, 1011,
	954,
};


const uint16_t tune_victory[] PROGMEM = {
	_T(TONE_C5, D_1_16),
	_T(0, D_1_32),
	_T(TONE_C5, D_1_16),
	_T(0, D_1_32),
	_T(TONE_C5, D_1_16),
	_T(0, D_1_32),
	_T(TONE_C5, D_1_4),
	_T(TONE_Bf4, D_1_4),
	_T(TONE_G4, D_1_4),
	_T(TONE_C5, D_1_8),
	_T(TONE_G4, D_1_8),
	_T(TONE_C5, D_1_1),
	_T(0, 0),
};

const uint16_t tune_cast[] PROGMEM = {
	_T(TONE_F4, D_1_32),
	_T(TONE_E4, D_1_32),
	_T(TONE_D4, D_1_32),
	_T(TONE_C3, D_1_32),
	_T(TONE_B3, D_1_32),
	_T(TONE_A3, D_1_32),
	_T(TONE_G3, D_1_32),
	_T(TONE_F3, D_1_32),
	_T(TONE_E3, D_1_32),
	_T(TONE_D3, D_1_32),
	_T(0, D_2_8),
	_T(TONE_D2, D_1_8),
	_T(0, 0)
};


